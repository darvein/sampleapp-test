def BUILD_VER = ""
def BUILD_NAME = ""

node{
    def timestamp = sh(script: 'echo -n `date +%s`', returnStdout: true)
    def release = "1.0.0"
    def releaseTag = release + "-" + timestamp

    env.NODEJS_HOME = "${tool 'node-8.x'}"
    env.PATH="${env.NODEJS_HOME}/bin:${env.PATH}"

    env.GC_PROJECT="nextbrave-187703"
	env.GC_ZONE="us-west1-a"
	env.GC_KUBENAME="volterra-ops"

	sh "gcloud auth activate-service-account --key-file=/gcloud-key.json"
    sh "gcloud config set project ${GC_PROJECT}"
    sh "gcloud config set compute/zone ${GC_ZONE}"
    sh "gcloud config set container/cluster ${GC_KUBENAME}"
    sh "gcloud container clusters get-credentials ${GC_KUBENAME}"

    ws(pwd()){
        stage ('Checkout') {
            echo "Checkout code"
            checkout scm
        }

        stage ('Build'){
            echo "Building code"
            sh 'npm install'
            sh 'npm run build'
        }

        stage ('Unit tests'){
		gitlabCommitStatus{
			echo "Running unit tests"
			sh 'npm run test'
		}
        }

        stage ('Archive'){
            echo "Archiving code (not implemented)"
        }

        stage ('Build image'){
            echo "Building docker image"
            sh "make build VERSION=${releaseTag}"

            echo "Uploading docker image"
            sh "make release VERSION=${releaseTag}"
        }

        stage ('Deploying'){
            echo "Deploying docker image"
            sh "cd helm && make upgrade VERSION=${releaseTag}"
        }

        stage ('Test'){
            echo "Waiting for application to be online"
            echo "Validating web endpoints"
	    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'saucelabs-credentials',
			    usernameVariable: 'SAUCE_USER', passwordVariable: 'SAUCE_KEY']]) {
		    sh "cat .env"
		    sh 'npm run test-ui'
	    }
        }

        stage ('Notify'){
            echo "Sending notifications  (not implemented)"
        }
    }
}
