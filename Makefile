GCR = us.gcr.io
VERSION = 1.0.0
LOCAL_PORT = 3000
EXPOSE_PORT = 3000

SERVICE_NAME = "sample-nodejs"
GC_PROJECT = `gcloud config get-value project`
IMAGE_NAME = $(GCR)/$(GC_PROJECT)/$(SERVICE_NAME)

.PHONY: all pre_build build tag_latest release run deploy

all: pre_build build

pre_build:
	@echo "[+] Pre-Build: Copy the APPLICATION source files"
	cp -R ../app/ ./src

build:
	@echo "[+] Building $(IMAGE_NAME):$(VERSION)"
	docker build -t "$(IMAGE_NAME):$(VERSION)" --rm .

tag_latest:
	@echo "[+] Tagging $(IMAGE_NAME) as latest"
	docker tag "$(IMAGE_NAME):$(VERSION)" "$(IMAGE_NAME):latest"

run: stop
	@echo "[+] Running $(SERVICE_NAME)"
	docker run --name $(SERVICE_NAME) -p $(LOCAL_PORT):$(EXPOSE_PORT) -it -d "$(IMAGE_NAME):$(VERSION)"

stop:
	@echo "[+] Stopping and destroing $(SERVICE_NAME)"
	@if docker ps | grep $(SERVICE_NAME); then echo "[+] Shuting down $(SERVICE_NAME)"; docker stop $(SERVICE_NAME); docker rm $(SERVICE_NAME); fi

release: tag_latest
	@if ! docker images $(IMAGE_NAME) | awk '{ print $$2 }' | grep -q -F $(VERSION); then echo "$(IMAGE_NAME) version $(VERSION) is not yet built. Please run 'make build'"; false; fi
	@echo "[+]  Releasing $(IMAGE_NAME) to Registry"
	gcloud docker -- push $(IMAGE_NAME)

deploy:
	@echo "[+] Deploying $(IMAGE_NAME):latest"
