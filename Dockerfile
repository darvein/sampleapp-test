FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY ./src/package*.json .

COPY ./package*.json /usr/src/app/
COPY ./dist /usr/src/app/dist

EXPOSE 3000
CMD [ "npm", "start" ]