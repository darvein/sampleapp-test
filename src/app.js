var express = require('express');
var path = require('path');

var SERVER_PORT = 3000;

var app = express();

app.configure(function () {
    app.use(express.logger('dev'));

    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.static('static'));
});

// Routes
app.get('/', function (req, res) {
    var html = buildHtml(req);

    res.writeHead(200, {
        'Content-Type': 'text/html',
        'Content-Length': html.length,
        'Expires': new Date().toUTCString()
    });
    res.end(html);
});

function buildHtml(req) {
    var header = '';
    var body = '';

    var title = 'Hello world Version 1.0 from Volterra';
    var message = 'This is a sample message #3';

    body = body + '<h1>' + title + '</h1>';
    body = body + '<h4>' + message + '</h4>';

    header = header + '<title>I am a page title - Sauce Labs</title>';

    return '<html><head>' + header + '</head><body>' + body + '</body></html>';
};

// Run server
app.listen(SERVER_PORT);
console.log('Your app is now running at: http://127.0.0.1:' + SERVER_PORT);
