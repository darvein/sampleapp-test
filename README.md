# Sample NODE JS application

Here are required steps to build, run and deploy this Sample NodeJS app.

###### Install the libraries and dependencies
```
npm install
```

Set SAUCE LABS credentials and HOST URL
```
export SAUCE_USER=your_saucelabs_username_here
export SAUCE_KEY=your_saucelabs_access_key_here
```

###### Build the application
```
npm run build
```

###### Test the application
```
npm run test
```

###### Test the application with Sauce Labs
```
npm run test-ui
```

###### Start the application
```
npm run start
```

## Docker image

In order to generate a docker image for the sample app, build it first:
```
make build
```

If you need to run the image:
```
make run
```

In order to push the image to Google Cloud repository
```
make release
``` 

## Jenkins integration

This repository contains a `Jenkinsfile` which contains all instructions for jenkins in order to:
- Build
- Test
- Build docker image
- Push docker image
- Deploy image to kubernetes
